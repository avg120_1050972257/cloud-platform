package com.ace.cache;

import java.util.ArrayList;
import java.util.List;

public class TestC {
    public static void main(String[] args) {
        System.out.println("Integer.toBinaryString(01)=" + Integer.toBinaryString(01));
        System.out.println("Integer.toBinaryString(012)=" + Integer.toBinaryString(012));
        System.out.println("Integer.toBinaryString(10)=" + Integer.toBinaryString(10));
        System.out.println("Integer.toBinaryString(0xa)=" + Integer.toBinaryString(0xa));
        System.out.println("Integer.toString(0x12)=" + Integer.toString(0x12));


        System.out.println("Integer.toBinaryString(0xa)=" + Integer.toBinaryString(0x68));
        System.out.println("Integer.toString(0xa)=" + Integer.toString(0x68));

        //c2666968686868686a7268686868daae
        //c2666968686868686a7368686868e76e
        //c2666968686868686a7a686868683b6f
        //c2666968686868686a7b6868686806af
        //c2666968686868686a71686868689eae
        //c266696868686868699f68686868768a
        //c266696868686868699b68686868874a
        //c266696868686868699c68686868328a
        //c266696868686868699e686868684b4a
        //c266696868686868699068686868228b
        System.out.println("Integer.toBinaryString(7)=" + Integer.toBinaryString(7));
        System.out.println("Integer.toBinaryString(9)=" + Integer.toBinaryString(9));
        System.out.println("7^9=" + (7 ^ 9));
        System.out.println("Integer.toBinaryString(7^9)=" + Integer.toBinaryString(7 ^ 9));
        System.out.println("Integer.toBinaryString(7)=" + Integer.toBinaryString(0xa2));
        System.out.println("Integer.toBinaryString(9)=" + Integer.toBinaryString(0x68));
        System.out.println("0xa2^0x68=" + (0xa2 ^ 0x68));
        System.out.println("Integer.toBinaryString(0xa2^0x68=)=" + Integer.toBinaryString(0xa2 ^ 0x68));
        System.out.println("0xa2^0x68=" + (0xa2 ^ 0x68 ^ 0x68));
        System.out.println("Integer.toBinaryString(0xa2^0x68^0x68)=" + Integer.toBinaryString(0xa2 ^ 0x68 ^ 0x68));


        System.out.println("Integer.toBinaryString(199)=" + Integer.toBinaryString(199));
        System.out.println("Integer.toBinaryString(0x68)=" + Integer.toBinaryString(0x68));
        System.out.println("199^0x68=" + (199 ^ 0x68));
        System.out.println("Integer.toBinaryString(199^0x68=)=" + Integer.toBinaryString(199 ^ 0x68));
        System.out.println("199^0x68=" + (199 ^ 0x68 ^ 0x68));
        System.out.println("Integer.toBinaryString(199^0x68^0x68)=" + Integer.toBinaryString(199 ^ 0x68 ^ 0x68));
        System.out.println("Integer.toHexString(199)=" + Integer.toHexString(199));

        System.out.println("Integer.toBinaryString(0xc7)=" + Integer.toBinaryString(0xc7));
        System.out.println("Integer.toBinaryString(0x68)=" + Integer.toBinaryString(0x68));
        System.out.println("0xc7^0x68=" + (0xc7 ^ 0x68));
        System.out.println("Integer.toBinaryString(c7^0x68=)=" + Integer.toBinaryString(0xc7 ^ 0x68));
        System.out.println("0xc7^0x68=" + (0xc7 ^ 0x68 ^ 0x68));
        System.out.println("Integer.toBinaryString(0xc7^0x68^0x68)=" + Integer.toBinaryString(0xc7 ^ 0x68 ^ 0x68));


        //0000000000022200000000
        System.out.println("Integer.toBinaryString(546)=" + Integer.toBinaryString(546));
        System.out.println("Integer.toBinaryString(0x68)=" + Integer.toBinaryString(0x68));
        System.out.println("546^0x68=" + (546 ^ 0x68));
        System.out.println("Integer.toBinaryString(546^0x68=)=" + Integer.toBinaryString(546 ^ 0x68));
        System.out.println("546^0x68=" + (546 ^ 0x68 ^ 0x68));
        System.out.println("Integer.toBinaryString(546^0x68^0x68)=" + Integer.toBinaryString(546 ^ 0x68 ^ 0x68));
        System.out.println("Integer.toHexString(546)=" + Integer.toHexString(546));


//0000000000022300000000
        System.out.println("Integer.toBinaryString(547)=" + Integer.toBinaryString(0xbf));
        System.out.println("Integer.toBinaryString(0x68)=" + Integer.toBinaryString(0x68));
        System.out.println("0xbf^0x68=" + (0xbf ^ 0x68));
        System.out.println("Integer.toBinaryString(0xbf^0x68=)=" + Integer.toBinaryString(0xbf ^ 0x68));
        System.out.println("0xbf^0x68=" + (0xbf ^ 0x68 ^ 0x68));
        System.out.println("Integer.toBinaryString(0xbf^0x68^0x68)=" + Integer.toBinaryString(0xbf ^ 0x68 ^ 0x68));
        System.out.println("Integer.toHexString(0xbf)=" + Integer.toHexString(0xbf));

        System.out.println("Integer.toBinaryString(254)=" + Integer.toBinaryString(254));
        System.out.println("Integer.toBinaryString(0x68)=" + Integer.toBinaryString(0x68));
        System.out.println("254^0x68=" + (254 ^ 0x68));
        System.out.println("Integer.toBinaryString(254^0x68=)=" + Integer.toBinaryString(254 ^ 0x68));
        System.out.println("254^0x68=" + (254 ^ 0x68 ^ 0x68));
        System.out.println("Integer.toBinaryString(254^0x68^0x68)=" + Integer.toBinaryString(254 ^ 0x68 ^ 0x68));
        System.out.println("Integer.toHexString(254)=" + Integer.toHexString(254));
//        System.out.println("this.to_decimal(1000100011)=" + to_decimal("1000100011"));
//        System.out.println("this.to_decimal(1000100011)=" + turn2to16("1000100011"));
//        System.out.println("this.to_decimal(1000100011)=" + f("4669726d77617265205265766973696f6e00"));
//        System.out.println("this.to_decimal(1000100011)=" + Integer.valueOf("1000100011",2));


        //   System.out.println("this.to_decimal(1000100011)=" + new String(hexStringToByte("5000a0000000e803")));
    }

    //十进制转2进制
    public static String to_binary(int orain) {
        List<Integer> list = new ArrayList<>();

        while (orain != 0) {
            int num = orain % 2;
            orain = orain / 2;
            list.add(num);
        }
        String bin = null;
        if (list != null) {
            bin = list.get(list.size() - 1) + "";
            for (int i = list.size() - 2; i >= 0; i--) {
                bin = bin + list.get(i);
            }
        }
        return bin;//这里我是用string类型来保存二进制数的
    }

    //二进制转10进制
    public static double to_decimal(String binary) {

        char bin[] = binary.toCharArray();
        List<Integer> list = new ArrayList<>();

        for (char c : bin) {
            list.add(Integer.parseInt("" + c));//把字符串转成整数
        }

        double num = 0;
        int index = list.size() - 1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == 1) {
                num = num + Math.pow(2, index - i);//2的index - 1 次方
            }
        }

        return num;
    }


    //2进制转16进制
    private static String turn2to16(String str) {
        String sum = "";

        int t = str.length() % 4;
        if (t != 0) {
            for (int i = str.length(); i - 4 >= 0; i = i - 4) {
                String s = str.substring(i - 4, i);
                int tem = Integer.parseInt(String.valueOf(s), 2);
                sum = Integer.toHexString(tem).toUpperCase() + sum;
            }
            String st = str.substring(0, t);

            int tem = Integer.parseInt(String.valueOf(st), 2);
            sum = Integer.toHexString(tem).toUpperCase() + sum;

        } else {
            for (int i = str.length(); i - 4 >= -1; i = i - 4) {
                String s = str.substring(i - 4, i);
                int tem = Integer.parseInt(String.valueOf(s), 2);
                sum = Integer.toHexString(tem).toUpperCase() + sum;
            }
        }
        return sum;

    }


    private static String f(String string) {
        // TODO Auto-generated method stub
        String s = "";
        for (int i = 0; i < string.length(); i++) {
            s += g(string.charAt(i));
        }
        return s;
    }

    private static String g(char charAt) {
        // TODO Auto-generated method stub
        switch (charAt) {
            case '0':
                return "0000";
            case '1':
                return "0001";
            case '2':
                return "0010";
            case '3':
                return "0011";
            case '4':
                return "0100";
            case '5':
                return "0101";
            case '6':
                return "0110";
            case '7':
                return "0111";
            case '8':
                return "1000";
            case '9':
                return "1001";
            case 'A':
                return "1010";
            case 'B':
                return "1011";
            case 'C':
                return "1100";
            case 'D':
                return "1101";
            case 'E':
                return "1110";
            case 'F':
                return "1111";

        }
        return null;
    }

    public static byte[] hexStringToByte(String hex) {
        int len = (hex.length() / 2);
        byte[] result = new byte[len];
        char[] achar = hex.toCharArray();
        for (int i = 0; i < len; i++) {
            int pos = i * 2;
            result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
        }
        return result;
    }

    private static byte toByte(char c) {
        byte b = (byte) "0123456789abcdef".indexOf(c);
        return b;
    }

}
