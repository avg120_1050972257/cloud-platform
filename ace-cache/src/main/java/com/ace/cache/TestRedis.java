package com.ace.cache;

import com.ace.cache.service.IRedisService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootApplication
public class TestRedis {

    /**
     * ID生成器
     */
    @Autowired
    private IRedisService redisCacheService;


    /**
     * @throws Exception
     * @Description 测试生成分布式ID
     * @author butterfly
     * @date 2017年8月3日 下午3:29:20
     */
    @Test
    public void testGeneratorId() throws Exception {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        cachedThreadPool.execute(new Runnable() {
            public void run() {
                while (true)
                    System.out.println(redisCacheService.incr("log"));
            }
        });
        System.in.read();
    }
}
