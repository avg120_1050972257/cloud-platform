package com.github.wxiaoqi.security.gate.v2.controller;

import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ace on 2017/9/10.
 */
@Log4j2
@RestController
@RequestMapping("test")
public class HelloController {
    @RequestMapping(value = "hello", method = RequestMethod.GET)
    public ObjectRestResponse hello() throws Exception {
        log.info("gate hello world...");
        final String result = "gate hello world";
        return new ObjectRestResponse<>().data(result);
    }
}
