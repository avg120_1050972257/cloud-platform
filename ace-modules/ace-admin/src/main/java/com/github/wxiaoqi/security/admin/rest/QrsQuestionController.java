package com.github.wxiaoqi.security.admin.rest;


import com.ace.cache.service.IRedisService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.wxiaoqi.security.admin.biz.QrsQuestionBiz;
import com.github.wxiaoqi.security.admin.entity.QrsQuestion;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.Query;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:30
 */
@Log4j2
@RestController
@RequestMapping("question")
public class QrsQuestionController extends BaseController<QrsQuestionBiz, QrsQuestion> {
    @Value("${web.resource-host}")
    private String resourceHost;

    @Value("${web.upload-path}")
    public String location;
    /**
     * ID生成器
     */
    @Autowired
    private IRedisService redisCacheService;

    @RequestMapping(value = "commit", method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<QrsQuestion> add(@RequestBody QrsQuestion entity) {
        if (entity.getIsActive() != null && entity.getIsActive() == 0) {
            Calendar rightNow = Calendar.getInstance();
            int year = rightNow.get(Calendar.YEAR);
            if (StringUtils.isEmpty(entity.getSubTime())) {
                int month = rightNow.get(Calendar.MONTH) + 1;
                int day = rightNow.get(Calendar.DAY_OF_MONTH);
                int hour = rightNow.get(Calendar.HOUR_OF_DAY);
                int minute = rightNow.get(Calendar.MINUTE);
                String subTime = (month > 9 ? String.valueOf(month) : "0" + String.valueOf(month)) + (day > 9 ? String.valueOf(day) : "0" + String.valueOf(day))
                        + (hour > 9 ? String.valueOf(hour) : "0" + String.valueOf(hour)) + (minute > 9 ? String.valueOf(minute) : "0" + String.valueOf(minute));
                entity.setSubTime(subTime);
            }
            if (StringUtils.isEmpty(entity.getQuestionCode())) {
                String questionNumber = redisCacheService.incrByYear(year + "");
                entity.setQuestionCode((year - 2000) + entity.getSubTime() + questionNumber);
//                entity.setQuestionCode(questionCode);
            }

        }
        baseBiz.insertSelective(entity);
        return new ObjectRestResponse<QrsQuestion>().data(entity);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<QrsQuestion> get(@PathVariable String id) {
        ObjectRestResponse<QrsQuestion> entityObjectRestResponse = new ObjectRestResponse<>();
        Object o = baseBiz.getQrsQuestionById(id);
        entityObjectRestResponse.data((QrsQuestion) o);
        return entityObjectRestResponse;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<QrsQuestion> remove(@PathVariable String id) {
        baseBiz.deleteById(id);
        return new ObjectRestResponse<QrsQuestion>();
    }

    @RequestMapping(value = "/put/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ObjectRestResponse<QrsQuestion> update(@RequestBody QrsQuestion entity) {
        //提交
        if (entity.getIsActive() != null && entity.getIsActive() == 0) { // &&
            Calendar rightNow = Calendar.getInstance();
            int year = rightNow.get(Calendar.YEAR);
            if (StringUtils.isEmpty(entity.getSubTime())) {
                int month = rightNow.get(Calendar.MONTH) + 1;
                int day = rightNow.get(Calendar.DAY_OF_MONTH);
                int hour = rightNow.get(Calendar.HOUR_OF_DAY);
                int minute = rightNow.get(Calendar.MINUTE);
                String subTime = (month > 10 ? String.valueOf(month) : "0" + String.valueOf(month)) + (day > 10 ? String.valueOf(day) : "0" + String.valueOf(day))
                        + (hour > 10 ? String.valueOf(hour) : "0" + String.valueOf(hour)) + (minute > 10 ? String.valueOf(minute) : "0" + String.valueOf(minute));
                entity.setSubTime(subTime);
            }
            if (StringUtils.isEmpty(entity.getQuestionCode())) {
                String questionNumber = redisCacheService.incrByYear(year + "");
                entity.setQuestionCode((year - 2000) + entity.getSubTime() + questionNumber);
            }
        }
        //
        baseBiz.updateSelectiveById(entity);
        if (StringUtils.isNotEmpty(entity.getConclusion()) && entity.getConclusion().equals("重启")) {
            QrsQuestion oldQuestion = baseBiz.selectById(entity.getId());
            QrsQuestion qrsQuestion = new QrsQuestion();
            BeanUtils.copyProperties(oldQuestion, qrsQuestion);
            qrsQuestion.setId(null);
            qrsQuestion.setQuestionCode(oldQuestion.getQuestionCode() + "R");
            qrsQuestion.setStatus("待提交");
            qrsQuestion.setAttr1("0");
            qrsQuestion.setAttr2(oldQuestion.getId().toString());
            qrsQuestion.setIsActive(1);
            qrsQuestion.setSubTime(null);
            //qrsQuestion.setResponsibleDept(null);
            qrsQuestion.setQuickReaction(null);
            qrsQuestion.setManufactureSpot(null);
            qrsQuestion.setManufactureReason(null);
            qrsQuestion.setManufactureTeam(null);
            qrsQuestion.setOutflowReason(null);
            qrsQuestion.setOutflowTeam(null);
            qrsQuestion.setManufacturePersonnel(null);
            qrsQuestion.setOutflowPersonnel(null);
            qrsQuestion.setFlowDesc(null);
            qrsQuestion.setManufactureMeasure(null);
            qrsQuestion.setOutflowMeasure(null);
            qrsQuestion.setProofDesc(null);
            qrsQuestion.setProofImgs(null);
            qrsQuestion.setProcessTime(null);
            qrsQuestion.setProcessUser(null);
            qrsQuestion.setConfirmStatus(null);
            qrsQuestion.setScore(null);
            qrsQuestion.setConclusion(null);
            qrsQuestion.setConfirmUser(null);
            qrsQuestion.setConfirmTime(null);

            baseBiz.insertSelective(qrsQuestion);
            ObjectRestResponse<QrsQuestion> entityObjectRestResponse = new ObjectRestResponse<>();
            entityObjectRestResponse.data(qrsQuestion);
            return entityObjectRestResponse;
        }
        return new ObjectRestResponse<QrsQuestion>().data(entity);
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<QrsQuestion> list(@RequestBody QrsQuestion entity) {
        log.info("QrsQuestionController list :" + this.getClass().getName()
                + "@" + Integer.toHexString(this.hashCode()));
//        Example example = new Example(QrsQuestion.class);
//        example.setOrderByClause("question_code ASC");
//        List<QrsQuestion> questionList = baseBiz.selectByExample(example);
//        return new TableResultResponse<QrsQuestion>(questionList.size(), questionList);
        List<QrsQuestion> questionList = baseBiz.getQrsQuestions(entity);
        return new TableResultResponse<QrsQuestion>(questionList.size(), questionList);
    }

    @GetMapping("/excelout")
    public void excelOut(@RequestParam Map<String, Object> params, HttpServletResponse response) throws IOException {
//        //查询列表数据
        if (params != null) {
            params.remove("uritimestamp");
        }
        Query query = new Query(params);
        List<QrsQuestion> pageList = baseBiz.selectQuestionsByQuery(query);


//        //定义导出的excel名字
//        String excelName = "问题列表";
//
//        //获取需要转出的excel表头的map字段
//        LinkedHashMap<String, String> fieldMap = new LinkedHashMap<>();
//        fieldMap.put("id","key");
//        fieldMap.put("questionCode","code");

        //导出用户相关信息
//        new ExportExcelUtils().export(excelName,pageList,fieldMap,response);
//        //查询列表数据
//        Query query = new Query(params);
//        List<QrsQuestion> pageList =  baseBiz.selectQuestionsByQuery(query);
//
        // 创建一个工作薄
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建一个sheet
        HSSFSheet sheet = wb.createSheet("sheet1");

        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();


        HSSFRow row = sheet.createRow(0);
        HSSFCellStyle style = wb.createCellStyle();
        //设置水平对齐的样式为居中对齐;
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        //设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        String[] excelHeader = {"序号", "问题编号", "提交时间", "问题大类", "发现地点", "发现者", "项目名称", "总成产品"
                , "重复发生", "缺陷部位", "缺陷类型", "缺陷名称", "缺陷数量", "外部围堵", "指标影响", "客户声音", "内部围堵",
                "许可时间", "问题管理", "产生地点", "制造原因", "流出原因", "制造班组", "制造者", "流出班组", "流出者",
                "状态", "快反", "流程", "证据", "确认", "结论", "评分", "产品缺陷图片"};
        for (int i = 0; i < excelHeader.length; i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue(excelHeader[i]);
            cell.setCellStyle(style);
            sheet.autoSizeColumn(i);
            //设置列宽
            //sheet.setColumnWidth(i, 256*30+184);
            // sheet.SetColumnWidth(i, 100 * 256);
            //256
            if (excelHeader[i].equals("序号")) {
                sheet.setColumnWidth(i, 10 * 256);
            } else if (excelHeader[i].equals("图片")) {
                sheet.setColumnWidth(i, 40 * 256);
                sheet.addMergedRegion(new CellRangeAddress(
                        0, //first row (0-based)  from 行
                        0, //last row  (0-based)  to 行
                        i, //first column (0-based) from 列
                        i + 10  //last column  (0-based)  to 列
                ));
            } else {
                sheet.setColumnWidth(i, 30 * 256);
            }

        }


        List<QrsQuestion> list = pageList;
        short width = 30;
        short height = 35;
        for (int i = 0; i < list.size(); i++) {
            row = sheet.createRow(i + 1);
            //row.setHeight((short) (35.7*30));
            //19.92
            row.setHeight((short) 637);
            QrsQuestion question = list.get(i);
            //设置样式 每列都是水平垂直居中
            HSSFCell cell = row.createCell(0);
            cell.setCellValue(i + 1);
            cell.setCellStyle(style);

            HSSFCell cell1 = row.createCell(1);
            cell1.setCellValue(question.getQuestionCode());
            cell1.setCellStyle(style);

            HSSFCell cell2 = row.createCell(2);
            cell2.setCellValue(question.getSubTime());
            cell2.setCellStyle(style);

            HSSFCell cell3 = row.createCell(3);
            cell3.setCellValue(question.getQuestionType());
            cell3.setCellStyle(style);

            HSSFCell cell4 = row.createCell(4);
            cell4.setCellValue(question.getFindSpot());
            cell4.setCellStyle(style);

            HSSFCell cell5 = row.createCell(5);
            cell5.setCellValue(question.getFindUser());
            cell5.setCellStyle(style);

            HSSFCell cell6 = row.createCell(6);
            cell6.setCellValue(question.getProjectName());
            cell6.setCellStyle(style);

            HSSFCell cell7 = row.createCell(7);
            cell7.setCellValue(question.getProductName());
            cell7.setCellStyle(style);

            HSSFCell cell8 = row.createCell(8);
            cell8.setCellValue(question.getManufacturePersonnel());
            cell8.setCellStyle(style);

            HSSFCell cell9 = row.createCell(9);
            cell9.setCellValue(question.getDefectivePart());
            cell9.setCellStyle(style);

            HSSFCell cell10 = row.createCell(10);
            cell10.setCellValue(question.getDefectiveType());
            cell10.setCellStyle(style);

            HSSFCell cell11 = row.createCell(11);
            cell11.setCellValue(question.getDefectiveName());
            cell11.setCellStyle(style);

            HSSFCell cell12 = row.createCell(12);
            cell12.setCellValue(question.getDefectiveNum() != null ? question.getDefectiveNum().toString() : "");
            cell12.setCellStyle(style);

            HSSFCell cell13 = row.createCell(13);
            cell13.setCellValue(question.getContainmentAction());
            cell13.setCellStyle(style);

            HSSFCell cell14 = row.createCell(14);
            cell14.setCellValue(question.getImpactIndex());
            cell14.setCellStyle(style);

            HSSFCell cell15 = row.createCell(15);
            cell15.setCellValue(question.getCustomerVoice());
            cell15.setCellStyle(style);

            HSSFCell cell16 = row.createCell(16);
            cell16.setCellValue(question.getRequiredPerform());
            cell16.setCellStyle(style);

            HSSFCell cell17 = row.createCell(17);
            cell17.setCellValue(question.getPermissionTime());
            cell17.setCellStyle(style);

            HSSFCell cell18 = row.createCell(18);
            cell18.setCellValue(question.getProblemManagement());
            cell18.setCellStyle(style);

            HSSFCell cell19 = row.createCell(19);
            cell19.setCellValue(question.getManufactureSpot());
            cell19.setCellStyle(style);

            HSSFCell cell20 = row.createCell(20);
            cell20.setCellValue(question.getManufactureReason());
            cell20.setCellStyle(style);

            HSSFCell cell21 = row.createCell(21);
            cell21.setCellValue(question.getOutflowReason());
            cell21.setCellStyle(style);

            HSSFCell cell22 = row.createCell(22);
            cell22.setCellValue(question.getManufactureTeam());
            cell22.setCellStyle(style);

            HSSFCell cell23 = row.createCell(23);
            cell23.setCellValue(question.getManufacturePersonnel());
            cell23.setCellStyle(style);

            HSSFCell cell24 = row.createCell(24);
            cell24.setCellValue(question.getOutflowTeam());
            cell24.setCellStyle(style);

            HSSFCell cell25 = row.createCell(25);
            cell25.setCellValue(question.getOutflowPersonnel());
            cell25.setCellStyle(style);

            HSSFCell cell26 = row.createCell(26);
            cell26.setCellValue(question.getStatus());
            cell26.setCellStyle(style);

            HSSFCell cell27 = row.createCell(27);
            cell27.setCellValue(question.getQuickReaction() != null ? question.getQuickReaction().toString() : "");
            cell27.setCellStyle(style);

            HSSFCell cell28 = row.createCell(28);
            cell28.setCellValue(question.getFlowDesc());
            cell28.setCellStyle(style);

            HSSFCell cell29 = row.createCell(29);
            cell29.setCellValue(question.getProofDesc());
            cell29.setCellStyle(style);

            HSSFCell cell30 = row.createCell(30);
            cell30.setCellValue(question.getConfirmStatus());
            cell30.setCellStyle(style);

            HSSFCell cell31 = row.createCell(31);
            cell31.setCellValue(question.getConclusion());
            cell31.setCellStyle(style);

            HSSFCell cell32 = row.createCell(32);
            if (question.getScore() != null) {
                cell32.setCellValue(question.getScore());
            } else {
                cell32.setCellValue("");
            }
            cell32.setCellStyle(style);

            String defectiveImgs = question.getDefectiveImgs();
            if (StringUtils.isNotEmpty(defectiveImgs) && !defectiveImgs.equals("[]")) {
                JSONArray defectiveImgArrs = JSON.parseArray(defectiveImgs);
                for (int j = 0; j < defectiveImgArrs.size(); j++) {
                    String img = defectiveImgArrs.get(j).toString();
                    log.info("img:" + img + " path:" + location + img.replace(resourceHost, ""));
                    File imgFile = new File(location + img.replace(resourceHost, ""));
                    BufferedImage bufferImg;//图片一
                    ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
                    //读图片
                    bufferImg = ImageIO.read(imgFile);
                    String fileSuffix = img.substring(img.length() - 3);
                    log.info("img fileSuffix:" + fileSuffix);
                    ImageIO.write(bufferImg, fileSuffix, byteArrayOut);

                    /**
                     * 该构造函数有8个参数
                     * 前四个参数是控制图片在单元格的位置，分别是图片距离单元格left，top，right，bottom的像素距离
                     * 后四个参数，前两个表示图片左上角所在的cellNum和 rowNum，后两个参数对应的表示图片右下角所在的cellNum和 rowNum，
                     * excel中的cellNum和rowNum的index都是从0开始的
                     */
                    HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0,
                            (short) (33 + j), (i + 1), (short) (34 + j), (i + 2));
                    anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_DONT_RESIZE);
//                int MOVE_AND_RESIZE = 0;//跟随单元格扩大或者缩小,就是你拖动单元格的时候,图片大小也在变
//                int MOVE_DONT_RESIZE = 2;//图片固定在该单元格在左上角,并且随着单元格移动
//                int DONT_MOVE_AND_RESIZE = 3;//固定在Excel某个位置,像牛皮广告一样不会动
                    // 插入图片
                    patriarch.createPicture(anchor, wb.addPicture(byteArrayOut
                            .toByteArray(), HSSFWorkbook.PICTURE_TYPE_JPEG)).resize(0.9);
                }

            } else {
                HSSFCell cell33 = row.createCell(33);
                cell33.setCellValue("");
                cell33.setCellStyle(style);
            }
        }

//        response.setContentType("application/vnd.ms-excel");
//        //设置文件名称
//        response.setHeader("Content-disposition", "attachment;filename=export.xls");
//        OutputStream outputStream = response.getOutputStream();
//        wb.write(outputStream);
//        outputStream.flush();
//        outputStream.close();
        //try catch这些关闭流略

        //准备将Excel的输出流通过response输出到页面下载
        //八进制输出流
        // response.setContentType("application/octet-stream");
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.addHeader("Content-Disposition", "attachment;filename=fileName" + ".xls");

        //刷新缓冲
        response.flushBuffer();

        //workbook将Excel写入到response的输出流中，供页面下载
        wb.write(response.getOutputStream());

        response.getOutputStream().close();
    }

    public static void main(String[] args) {
        String defectiveImgs = "[\"https://www.frcqrs.cn/uploadFile/uploadFile/1575779252922.jpg\",\"https://www.frcqrs.cn/uploadFile/uploadFile/1575779186166.jpg\"]";
        JSONArray defectiveImgArrs = JSON.parseArray(defectiveImgs);
        String firstImg = defectiveImgArrs.get(0).toString();
        System.out.println(firstImg.replace("https://www.frcqrs.cn/uploadFile", ""));
//        File imgFile = new File(location +firstImg.replace(resourceHost,"")) ;
    }
}
