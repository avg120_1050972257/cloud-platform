package com.github.wxiaoqi.security.admin.biz;

import com.github.wxiaoqi.security.admin.entity.UploadFile;
import com.github.wxiaoqi.security.admin.mapper.UploadFileMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UploadFileBiz extends BaseBiz<UploadFileMapper, UploadFile> {


    @Override
    public int insertSelective(UploadFile entity) {
        return super.insertSelective(entity);
    }

    @Override
    public int updateSelectiveById(UploadFile entity) {
        return super.updateSelectiveById(entity);
    }


}
