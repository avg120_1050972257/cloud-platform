package com.github.wxiaoqi.security.admin.rest;


import com.github.wxiaoqi.security.admin.biz.WarningRecordBiz;
import com.github.wxiaoqi.security.admin.entity.QrsQuestion;
import com.github.wxiaoqi.security.admin.entity.WarningRecord;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:30
 */
@Log4j2
@RestController
@RequestMapping("warning-record")
public class WarningRecordController extends BaseController<WarningRecordBiz, WarningRecord> {
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<WarningRecord> list(@RequestBody WarningRecord entity) {
        entity.setLocked(0);
        List<WarningRecord> warningRecordList = baseBiz.selectList(entity);
        return new TableResultResponse<WarningRecord>(warningRecordList.size(), warningRecordList);
    }

    @RequestMapping(value = "/delWarnings", method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<WarningRecord> updateLocked(@RequestBody WarningRecord entity) {
        baseBiz.uploadLocked(entity);
        return new ObjectRestResponse<WarningRecord>().data(entity);
    }
}
