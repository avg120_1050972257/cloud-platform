package com.github.wxiaoqi.security.admin.rest;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.admin.biz.DictTypeBiz;
import com.github.wxiaoqi.security.admin.biz.DictValueBiz;
import com.github.wxiaoqi.security.admin.entity.DictType;
import com.github.wxiaoqi.security.admin.entity.DictValue;
import com.github.wxiaoqi.security.common.msg.ListRestResponse;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:30
 */
@Log4j2
@RestController
@RequestMapping("dict-value")
public class DictValueController extends BaseController<DictValueBiz, DictValue> {
    @Autowired
    DictTypeBiz dictTypeBiz;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<DictValue> page(@RequestParam(defaultValue = "10") int limit,
                                               @RequestParam(defaultValue = "1") int offset, String name, @RequestParam(defaultValue = "0") int typeId) {
        Example example = new Example(DictValue.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("typeId", typeId);
        if (StringUtils.isNotBlank(name)) {
            criteria.andLike("name", "%" + name + "%");
        }
        Page<Object> result = PageHelper.startPage(offset, limit);
        List<DictValue> dictValues = baseBiz.selectByExample(example);
        return new TableResultResponse<DictValue>(dictValues.size(), dictValues);
    }

    /**
     * 添加其他选项 等待审核
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "addDictValue", method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<DictValue> addDictValue(@RequestBody DictValue entity) {
//        DictType dictType = dictTypeBiz.getDictTypeByCode(entity.getTypeCode());
//        if(dictType!=null){
//            entity.setTypeId(dictType.getId());
        entity.setIsActive(1);
        baseBiz.insertSelective(entity);
//        }
        return new ObjectRestResponse<DictValue>().data(entity);
    }

    @RequestMapping(value = "/objects/{typeCode}", method = RequestMethod.GET)
    @ResponseBody
    public ListRestResponse getDictValues(@PathVariable String typeCode) {
        List<DictValue> dictValues = baseBiz.getDictValuesByTypeCode(typeCode);
        return new ListRestResponse<DictValue>().data(dictValues);
    }
}
