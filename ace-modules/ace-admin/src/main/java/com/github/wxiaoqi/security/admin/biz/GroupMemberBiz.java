package com.github.wxiaoqi.security.admin.biz;

import com.github.wxiaoqi.security.admin.entity.GroupMember;
import com.github.wxiaoqi.security.admin.mapper.GroupMemberMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GroupMemberBiz extends BaseBiz<GroupMemberMapper, GroupMember> {


    @Override
    public int insertSelective(GroupMember entity) {
        return super.insertSelective(entity);
    }

    @Override
    public int updateSelectiveById(GroupMember entity) {
        return super.updateSelectiveById(entity);
    }

    public GroupMember getGroupMemberByUserId(int userId) {
        return mapper.getGroupMemberByUserId(userId);
    }

    public List<GroupMember> getRolesByUserId(int userId) {
        return mapper.getRolesByUserId(userId);
    }

}
