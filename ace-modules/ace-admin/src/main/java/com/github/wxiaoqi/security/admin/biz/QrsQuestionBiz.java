package com.github.wxiaoqi.security.admin.biz;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.admin.entity.QrsQuestion;
import com.github.wxiaoqi.security.admin.mapper.QrsQuestionMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.util.Query;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:27
 */
@Log4j2
@Service
@Transactional(rollbackFor = Exception.class)
public class QrsQuestionBiz extends BaseBiz<QrsQuestionMapper, QrsQuestion> {


    @Override
    public int insertSelective(QrsQuestion entity) {
        return super.insertSelective(entity);
    }

    @Override
    public int updateSelectiveById(QrsQuestion entity) {
        return super.updateSelectiveById(entity);
    }

    public QrsQuestion getQrsQuestionById(String id) {
        return mapper.getQrsQuestionById(id);
    }

    public List<QrsQuestion> getQrsQuestions(QrsQuestion entity) {
        log.info("QrsQuestionBiz getQrsQuestions :" + this.getClass().getName()
                + "@" + Integer.toHexString(this.hashCode()));
        return mapper.getQrsQuestions(entity.getCommitUser(), entity.getProcessUser(), entity.getConfirmUser(), entity.getDays(), entity.getIsActive(), entity.getStatus(), entity.getConfirmStatus());
    }

    public List<QrsQuestion> selectQuestionsByQuery(Query query) {
        Class<QrsQuestion> clazz = (Class<QrsQuestion>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        Example example = new Example(clazz);
        if (query.entrySet().size() > 0) {
            Example.Criteria criteria = example.createCriteria();
            for (Map.Entry<String, Object> entry : query.entrySet()) {
                criteria.andLike(entry.getKey(), "%" + entry.getValue().toString() + "%");
            }
        }
//        Example.Criteria criteria = example.createCriteria();
//        criteria.andEqualTo("locked",0);
        if (query.getPage() > 0) {
            Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
        }
        List<QrsQuestion> list = mapper.selectByExample(example);
        return list;
    }

    public List<QrsQuestion> getTestQuestionList() {
        return mapper.getTestQuestionList();
    }
}
