package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.DictValue;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface DictValueMapper extends Mapper<DictValue> {
    public List<DictValue> getDictValuesByTypeId(@Param("typeId") int typeId);

    public List<DictValue> getDictValuesByTypeCode(@Param("typeCode") String typeCode);

    @Select("select NOW()")
    public String getSqlDate();

    @Select("select count(*) from qrs_dict_value")
    public int getDictValueCount();

    public List<DictValue> getTestQuestionList();
}