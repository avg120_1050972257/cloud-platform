package com.github.wxiaoqi.security.admin.rest;


import com.github.wxiaoqi.security.admin.biz.GroupMemberBiz;
import com.github.wxiaoqi.security.admin.entity.GroupMember;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:30
 */
@Log4j2
@RestController
@RequestMapping("group-member")
public class GroupMemberController extends BaseController<GroupMemberBiz, GroupMember> {
    @RequestMapping(value = "/byUserId/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<GroupMember> getGroupMemberByUserId(@PathVariable int userId) {
        ObjectRestResponse<GroupMember> entityObjectRestResponse = new ObjectRestResponse<>();
        Object o = baseBiz.getGroupMemberByUserId(userId);
        entityObjectRestResponse.data((GroupMember) o);
        return entityObjectRestResponse;
    }

    @RequestMapping(value = "/roles/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public List<GroupMember> getRolesByUserId(@PathVariable int userId) {
        List<GroupMember> list = baseBiz.getRolesByUserId(userId);
        return list;
    }
}
