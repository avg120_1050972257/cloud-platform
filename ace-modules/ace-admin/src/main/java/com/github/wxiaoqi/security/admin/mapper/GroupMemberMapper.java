package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.GroupMember;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface GroupMemberMapper extends Mapper<GroupMember> {
    public GroupMember getGroupMemberByUserId(@Param("userId") int userId);

    public List<GroupMember> getRolesByUserId(@Param("userId") int userId);
}