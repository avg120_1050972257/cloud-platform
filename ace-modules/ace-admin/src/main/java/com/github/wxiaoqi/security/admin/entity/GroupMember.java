package com.github.wxiaoqi.security.admin.entity;

import com.github.wxiaoqi.security.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import javax.persistence.*;

@Setter
@Getter
@Table(name = "base_group_member")
public class GroupMember extends BaseEntity {

    @Column(name = "group_id")
    private String groupId;

    @Column(name = "user_id")
    private String userId;

    private String attr1;

    private String attr2;

    private String attr3;

    private String attr4;

    private String attr5;

    private String attr6;

    private String attr7;

    private String attr8;

    private Integer uritimestamp;
    @Transient
    private String groupCode;

    @Transient
    private String workingShift;

    @Transient
    private String questionSubDept;

}