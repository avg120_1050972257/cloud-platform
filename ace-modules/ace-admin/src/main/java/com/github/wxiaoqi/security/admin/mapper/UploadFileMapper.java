package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.UploadFile;
import tk.mybatis.mapper.common.Mapper;

public interface UploadFileMapper extends Mapper<UploadFile> {
}