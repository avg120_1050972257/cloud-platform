package com.github.wxiaoqi.security.admin.biz;

import com.github.wxiaoqi.security.admin.entity.GroupLeader;
import com.github.wxiaoqi.security.admin.mapper.GroupLeaderMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GroupLeaderBiz extends BaseBiz<GroupLeaderMapper, GroupLeader> {


    @Override
    public int insertSelective(GroupLeader entity) {
        return super.insertSelective(entity);
    }

    @Override
    public int updateSelectiveById(GroupLeader entity) {
        return super.updateSelectiveById(entity);
    }


}
