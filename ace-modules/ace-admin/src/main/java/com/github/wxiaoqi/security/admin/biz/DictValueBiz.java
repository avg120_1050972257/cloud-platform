package com.github.wxiaoqi.security.admin.biz;

import com.github.wxiaoqi.security.admin.entity.DictValue;
import com.github.wxiaoqi.security.admin.mapper.DictValueMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DictValueBiz extends BaseBiz<DictValueMapper, DictValue> {


    @Override
    public int insertSelective(DictValue entity) {
        return super.insertSelective(entity);
    }

    @Override
    public int updateSelectiveById(DictValue entity) {
        return super.updateSelectiveById(entity);
    }

    public List<DictValue> getDictValuesByTypeCode(String typeCode) {
        return mapper.getDictValuesByTypeCode(typeCode);
    }

    public String getSqlDate() {
        return mapper.getSqlDate();
    }

    public int getDictValueCount() {
        return mapper.getDictValueCount();
    }
}
