package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.WarningRecord;
import tk.mybatis.mapper.common.Mapper;

public interface WarningRecordMapper extends Mapper<WarningRecord> {
    public int uploadLocked(WarningRecord warningRecord);
}