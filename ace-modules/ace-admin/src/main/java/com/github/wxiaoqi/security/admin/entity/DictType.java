package com.github.wxiaoqi.security.admin.entity;

import com.github.wxiaoqi.security.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Getter
@Setter
@Table(name = "qrs_dict_type")
public class DictType extends BaseEntity {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "myql")
//    private Integer id;

    private String code;

    private String name;

//    private String locked;
//
//    private String description;
//
//    @Column(name = "crt_time")
//    private Date crtTime;
//
//    @Column(name = "crt_user")
//    private String crtUser;
//
//    @Column(name = "crt_name")
//    private String crtName;
//
//    @Column(name = "crt_host")
//    private String crtHost;
//
//    @Column(name = "upd_time")
//    private Date updTime;
//
//    @Column(name = "upd_user")
//    private String updUser;
//
//    @Column(name = "upd_name")
//    private String updName;
//
//    @Column(name = "upd_host")
//    private String updHost;

    private String attr1;

    private String attr2;

    private String attr3;

    private String attr4;

    private String attr5;

    private String attr6;

    private String attr7;

    private String attr8;

    private Integer uritimestamp;

    @Transient
    private List<DictValue> dictValueList;
}