package com.github.wxiaoqi.security.admin.biz;

import com.github.wxiaoqi.security.admin.entity.DictType;
import com.github.wxiaoqi.security.admin.mapper.DictTypeMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.enums.ResultEnum;
import com.github.wxiaoqi.security.common.exception.BaseException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DictTypeBiz extends BaseBiz<DictTypeMapper, DictType> {


    @Override
    public int insertSelective(DictType entity) {
        DictType dictType = new DictType();
        dictType.setCode(entity.getCode());
        dictType.setLocked(0);
        dictType = mapper.selectOne(dictType);
        if (dictType != null) {
            throw new BaseException(ResultEnum.DICT_ALREADY_EXISTED);
        }
        return super.insertSelective(entity);
    }

    @Override
    public int updateSelectiveById(DictType entity) {
        return super.updateSelectiveById(entity);
    }

    public List<DictType> getDictTypes() {
        return mapper.getDictTypes();
    }

    public DictType getDictTypeByCode(String code) {
        DictType dictType = new DictType();
        dictType.setCode(code);
        dictType.setLocked(0);
        dictType = mapper.selectOne(dictType);
        return dictType;
    }
}
