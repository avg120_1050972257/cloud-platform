package com.github.wxiaoqi.security.admin.entity;

import com.github.wxiaoqi.security.common.entity.BaseEntity;
import com.github.wxiaoqi.security.common.util.UUIdGenId;
import lombok.Getter;
import lombok.Setter;
import tk.mybatis.mapper.annotation.KeySql;

import java.util.Date;
import javax.persistence.*;

@Getter
@Setter
@Table(name = "qrs_question")
public class QrsQuestion {
    //extends BaseEntity
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    @Column(name = "defective_imgs")
    private String defectiveImgs;

    @Column(name = "product_imgs")
    private String productImgs;

    @Column(name = "project_name")
    private String projectName;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "defective_part")
    private String defectivePart;

    @Column(name = "defective_type")
    private String defectiveType;

    @Column(name = "multiple_production")
    private String multipleProduction;

    @Column(name = "defective_name")
    private String defectiveName;

    @Column(name = "defective_num")
    private Integer defectiveNum;

    @Column(name = "containment_action")
    private String containmentAction;

    @Column(name = "find_spot")
    private String findSpot;

    @Column(name = "find_user")
    private String findUser;

    @Column(name = "find_method")
    private String findMethod;

    @Column(name = "question_type")
    private String questionType;

    @Column(name = "responsible_dept")
    private String responsibleDept;

    @Column(name = "manufacture_dept")
    private String manufactureDept;

    @Column(name = "manufacture_spot")
    private String manufactureSpot;

    @Column(name = "impact_index")
    private String impactIndex;

    @Column(name = "customer_voice")
    private String customerVoice;

    @Column(name = "required_perform")
    private String requiredPerform;

    @Column(name = "permission_time")
    private String permissionTime;

    @Column(name = "manufacture_reason")
    private String manufactureReason;

    @Column(name = "outflow_reason")
    private String outflowReason;

    @Column(name = "manufacture_team")
    private String manufactureTeam;

    @Column(name = "manufacture_personnel")
    private String manufacturePersonnel;

    @Column(name = "outflow_team")
    private String outflowTeam;

    @Column(name = "outflow_personnel")
    private String outflowPersonnel;

    @Column(name = "problem_management")
    private String problemManagement;

    @Column(name = "sub_time")
    private String subTime;

    @Column(name = "question_code")
    private String questionCode;

    @Column(name = "question_submitter")
    private String questionSubmitter;

    @Column(name = "working_shift")
    private String workingShift;

    @Column(name = "question_sub_dept")
    private String questionSubDept;

    private String status;

    @Column(name = "quick_reaction")
    private Integer quickReaction;

    @Column(name = "flow_desc")
    private String flowDesc;

    @Column(name = "manufacture_measure")
    private String manufactureMeasure;

    @Column(name = "outflow_measure")
    private String outflowMeasure;

    @Column(name = "proof_desc")
    private String proofDesc;

    @Column(name = "proof_imgs")
    private String proofImgs;

    @Column(name = "confirm_status")
    private String confirmStatus;

    private Float score;

    private String conclusion;

    @Column(name = "process_time")
    private String processTime;

    @Column(name = "process_user")
    private String processUser;

    @Column(name = "confirm_time")
    private String confirmTime;

    @Column(name = "confirm_user")
    private String confirmUser;

    private Integer locked;

    private String description;

    @Column(name = "crt_time")
    private Date crtTime;

    @Column(name = "crt_user")
    private String crtUser;

    @Column(name = "crt_name")
    private String crtName;

    @Column(name = "crt_host")
    private String crtHost;

    @Column(name = "upd_time")
    private Date updTime;

    @Column(name = "upd_user")
    private String updUser;

    @Column(name = "upd_name")
    private String updName;

    @Column(name = "upd_host")
    private String updHost;

    @Column(name = "is_active")
    private Integer isActive;

    private String attr1;

    private String attr2;

    private String attr3;

    private String attr4;

    private String attr5;

    private String attr6;

    private String attr7;

    private String attr8;

    private Integer uritimestamp;

    @Transient
    private int days;
    @Transient
    private String commitUser;
    @Transient
    private String warningId;
    @Transient
    private String toPoint;
}