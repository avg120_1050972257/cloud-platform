package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.DictType;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface DictTypeMapper extends Mapper<DictType> {
    public DictType getDictTypeById(@Param("id") int id);

    public List<DictType> getDictTypes();
}