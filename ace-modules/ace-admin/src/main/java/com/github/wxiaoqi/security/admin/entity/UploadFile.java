package com.github.wxiaoqi.security.admin.entity;

import com.github.wxiaoqi.security.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Table(name = "qrs_upload_file")
public class UploadFile extends BaseEntity {
    /**
     * 主键
     */
//    @Id
//    private String id;
//
//    private String description;
//
//    @Column(name = "crt_time")
//    private Date crtTime;
//
//    @Column(name = "crt_user")
//    private String crtUser;
//
//    @Column(name = "crt_name")
//    private String crtName;
//
//    @Column(name = "crt_host")
//    private String crtHost;
//
//    @Column(name = "upd_time")
//    private Date updTime;
//
//    @Column(name = "upd_user")
//    private String updUser;
//
//    @Column(name = "upd_name")
//    private String updName;
//
//    @Column(name = "upd_host")
//    private String updHost;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Date lastModifiedDate;

    private String path;

    @Column(name = "ref_key")
    private String refKey;

    private String attr1;

    private String attr2;

    private String attr3;

    private String attr4;

    private String attr5;

    private String attr6;

    private String attr7;

    private String attr8;

    private Integer uritimestamp;

//    /**
//     * 是否锁定
//     */
//    private Integer locked;


}