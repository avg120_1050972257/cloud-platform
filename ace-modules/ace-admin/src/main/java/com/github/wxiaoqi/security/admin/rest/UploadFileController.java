package com.github.wxiaoqi.security.admin.rest;


import com.github.wxiaoqi.security.admin.biz.UploadFileBiz;
import com.github.wxiaoqi.security.admin.entity.UploadFile;
import com.github.wxiaoqi.security.admin.utils.FileUtil;
import com.github.wxiaoqi.security.common.enums.ResultEnum;
import com.github.wxiaoqi.security.common.exception.BaseException;
import com.github.wxiaoqi.security.common.rest.BaseController;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:30
 */
@Log4j2
@RestController
@RequestMapping("uploadFile")
public class UploadFileController extends BaseController<UploadFileBiz, UploadFile> {
    @Autowired
    FileUtil fileUtil;

    @Value("${web.resource-host}")
    private String resourceHost;

    /**
     * 上传
     */
    @RequestMapping("/uploadFile")
    public ResponseEntity<?> uploadFile(HttpServletRequest request, @RequestParam(value = "uploadFile", required = false) MultipartFile uploadFile) throws Exception {

        if (uploadFile == null) {
            throw new BaseException(ResultEnum.PARAM_IS_BLANK);
        }
        String relativePath = resourceHost + fileUtil.uploadFile(uploadFile, "uploadFile");
        UploadFile uploadFile1 = new UploadFile();
        uploadFile1.setPath(relativePath);
        uploadFile1.setFileName(uploadFile.getOriginalFilename());
        baseBiz.insertSelective(uploadFile1);
        return ResponseEntity.ok(uploadFile1);
    }
}
