package com.github.wxiaoqi.security.admin.biz;

import com.github.wxiaoqi.security.admin.entity.WarningRecord;
import com.github.wxiaoqi.security.admin.mapper.WarningRecordMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class WarningRecordBiz extends BaseBiz<WarningRecordMapper, WarningRecord> {


    @Override
    public int insertSelective(WarningRecord entity) {
        return super.insertSelective(entity);
    }

    @Override
    public int updateSelectiveById(WarningRecord entity) {
        return super.updateSelectiveById(entity);
    }

    public int uploadLocked(WarningRecord entity) {
        return mapper.uploadLocked(entity);
    }
}
