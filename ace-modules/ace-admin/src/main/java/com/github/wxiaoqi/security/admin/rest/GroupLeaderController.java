package com.github.wxiaoqi.security.admin.rest;


import com.github.wxiaoqi.security.admin.biz.GroupLeaderBiz;
import com.github.wxiaoqi.security.admin.entity.GroupLeader;
import com.github.wxiaoqi.security.common.rest.BaseController;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-23 20:30
 */
@Log4j2
@RestController
@RequestMapping("group-leader")
public class GroupLeaderController extends BaseController<GroupLeaderBiz, GroupLeader> {

}
