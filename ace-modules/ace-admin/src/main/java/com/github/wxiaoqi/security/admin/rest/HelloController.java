package com.github.wxiaoqi.security.admin.rest;

import com.github.wxiaoqi.security.admin.biz.DictValueBiz;
import com.github.wxiaoqi.security.admin.biz.QrsQuestionBiz;
import com.github.wxiaoqi.security.admin.entity.QrsQuestion;
import com.github.wxiaoqi.security.admin.utils.TimeUtils;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by ace on 2017/9/10.
 */
@Log4j2
@RestController
@RequestMapping("test")
public class HelloController {
    @Autowired
    DictValueBiz dctValueBiz;

    @Autowired
    QrsQuestionBiz qrsQuestionBiz;

    @RequestMapping(value = "hello", method = RequestMethod.GET)
    public ObjectRestResponse hello() throws Exception {
        log.info("admin hello world...");
        final String result = "admin hello world";
        return new ObjectRestResponse<>().data(result);
    }

    @RequestMapping(value = "getDate", method = RequestMethod.GET)
    public ObjectRestResponse getDate() throws Exception {
        log.info("admin getDate ...");
        final String result = TimeUtils.getCurrentTimeStr();
        return new ObjectRestResponse<>().data(result);
    }

    @RequestMapping(value = "getDictValueCount", method = RequestMethod.GET)
    public ObjectRestResponse getDictValueCount() throws Exception {
        log.info("admin getDictValueCount ...");
        final int result = dctValueBiz.getDictValueCount();
        return new ObjectRestResponse<>().data(result);
    }

    @RequestMapping(value = "getSqlDate", method = RequestMethod.GET)
    public ObjectRestResponse getSqlDate() throws Exception {
        log.info("admin getSqlDate ...");
        final String result = dctValueBiz.getSqlDate();
        return new ObjectRestResponse<>().data(result);
    }

    @RequestMapping(value = "getTestQuestionList", method = RequestMethod.GET)
    public ObjectRestResponse getTestQuestionList() throws Exception {
        log.info("admin getTestQuestionList ...");
        List<QrsQuestion> list = qrsQuestionBiz.getTestQuestionList();
        return new ObjectRestResponse<>().data(list);
    }

}
