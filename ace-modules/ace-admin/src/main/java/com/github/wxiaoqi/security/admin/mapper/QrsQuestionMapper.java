package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.QrsQuestion;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface QrsQuestionMapper extends Mapper<QrsQuestion> {
    public QrsQuestion getQrsQuestionById(@Param("id") String id);

    public List<QrsQuestion> getQrsQuestions(@Param("commitUser") String commitUser, @Param("processUser") String processUser
            , @Param("confirmUser") String confirmUser, @Param("days") int days, @Param("isActive") int isActive
            , @Param("status") String status
            , @Param("confirmStatus") String confirmStatus);

    @Select("SELECT DISTINCT t1.*,t2.to_point AS toPoint, \n" +
            "CASE\n" +
            "\t\t\n" +
            "\t\tWHEN t1.`status` = '已完成' THEN\n" +
            "\t\tquick_reaction \n" +
            "\t\tWHEN is_active = 1 THEN\n" +
            "\t\t'' ELSE GetQuickReaction ( question_code, sub_time ) \n" +
            "\tEND AS quick_reaction\t\n" +
            "FROM\n" +
            "\tqrs_question t1\n" +
            "\tLEFT JOIN qrs_warning_record t2 ON t1.id = t2.question_id \n" +
            "\tAND t1.attr1 = t2.qstatus \n" +
            "\tAND t2.locked = 0 \n" +
            "WHERE\n" +
            "\tt1.locked = 0 \n" +
            "\tAND t1.is_active <= 1 \n" +
            "\tAND t1.crt_user = 8 \n" +
            "\tAND ( GetDays ( t1.question_code, t1.sub_time ) <= 30 OR ( t1.question_code IS NULL ) )")
    public List<QrsQuestion> getTestQuestionList();
}