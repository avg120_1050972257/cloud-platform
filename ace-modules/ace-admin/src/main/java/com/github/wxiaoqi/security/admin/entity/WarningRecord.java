package com.github.wxiaoqi.security.admin.entity;

import com.github.wxiaoqi.security.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import javax.persistence.*;

@Getter
@Setter
@Table(name = "qrs_warning_record")
public class WarningRecord extends BaseEntity {

    @Column(name = "question_id")
    private String questionId;

    private String qstatus;

    @Column(name = "from_point")
    private String fromPoint;

    @Column(name = "to_point")
    private String toPoint;

    private String attr1;

    private String attr2;

    private String attr3;

    private String attr4;

    private String attr5;

    private String attr6;

    private String attr7;

    private String attr8;

    private Integer uritimestamp;
}