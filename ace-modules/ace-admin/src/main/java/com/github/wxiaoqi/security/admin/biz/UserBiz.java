package com.github.wxiaoqi.security.admin.biz;

import com.ace.cache.annotation.Cache;
import com.ace.cache.annotation.CacheClear;
import com.github.wxiaoqi.security.admin.entity.User;
import com.github.wxiaoqi.security.admin.mapper.MenuMapper;
import com.github.wxiaoqi.security.admin.mapper.UserMapper;
import com.github.wxiaoqi.security.auth.client.jwt.UserAuthUtil;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.constant.UserConstant;
import com.github.wxiaoqi.security.common.enums.ResultEnum;
import com.github.wxiaoqi.security.common.exception.BaseException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-08 16:23
 */
@Log4j2
@Service
@Transactional(rollbackFor = Exception.class)
public class UserBiz extends BaseBiz<UserMapper, User> {

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private UserAuthUtil userAuthUtil;

    @Override
    public int insertSelective(User entity) {
        String password = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(entity.getPassword());
        entity.setPassword(password);
        User user = new User();
        user.setUsername(entity.getUsername());
        user.setLocked(0);
        user = mapper.selectOne(user);
        if (user != null) {
            throw new BaseException(ResultEnum.USER_HAS_EXISTED);
        }
        return super.insertSelective(entity);
    }

    @Override
    @CacheClear(pre = "user{1.username}")
    public int updateSelectiveById(User entity) {
        if (entity.getPassword() != null) {
            String password = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(entity.getPassword());
            entity.setPassword(password);
        }
        return super.updateSelectiveById(entity);
    }

    /**
     * 根据用户名获取用户信息
     *
     * @param username
     * @return
     */
    @Cache(key = "user{1}")
    public User getUserByUsername(String username) {
        log.info(" UserBiz getUserByUsername start ...;username:" + username);
        User user = new User();
        user.setUsername(username);
        User userInfo = mapper.selectOne(user);
        log.info(" UserBiz getUserByUsername end ...;username:" + username);
        return userInfo;
    }


}
