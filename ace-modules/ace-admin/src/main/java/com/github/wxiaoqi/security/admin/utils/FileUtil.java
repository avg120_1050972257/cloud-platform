package com.github.wxiaoqi.security.admin.utils;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
@Log4j2
public class FileUtil {

    @Value("${web.upload-path}")
    public String location;

    /**
     * @param file    文件
     * @param subpath 子路径
     * @return 返回文件存储路径
     */
    public String uploadFile(MultipartFile file, String subpath) {
        String originalFileName = file.getOriginalFilename();
        String suffix = originalFileName.substring(originalFileName.lastIndexOf("."), originalFileName.length());
        String fileName = System.currentTimeMillis() + suffix;
        File dir = new File(location + "/" + subpath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File newFile = new File(dir, fileName);
        try {
            file.transferTo(newFile);
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e);
            throw new RuntimeException("文件写入出错", e);
        }
        return "/" + subpath + "/" + newFile.getName();
    }
}
