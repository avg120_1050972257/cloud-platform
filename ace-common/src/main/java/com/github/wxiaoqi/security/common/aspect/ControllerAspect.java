package com.github.wxiaoqi.security.common.aspect;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 * controller 切面处理
 *
 * @author wanghaobin
 * @description
 * @date 2017年5月18日
 * @since 1.7
 */
@Log4j2
@Aspect
@Component
public class ControllerAspect {

    ControllerAspect() {
        log.info("init ControllerAspect: ");
    }

    @Pointcut("execution(* com.github.wxiaoqi.security.*.controller.*.*(..))")
    public void pointController() {
        log.info("init pointController: ");
    }

    @Pointcut("execution(* com.github.wxiaoqi.security.*.rest.*.*(..))")
    public void pointRest() {
        log.info("init pointRest: ");
    }

    @Around("pointController()||pointRest()")
    public Object interceptor(ProceedingJoinPoint invocation)
            throws Throwable {
        MethodSignature signature = (MethodSignature) invocation.getSignature();
        Method method = signature.getMethod();
        Object result = null;
        Class<?>[] parameterTypes = method.getParameterTypes();
        Object[] arguments = invocation.getArgs();
        String key = "";
        String value = "";
        long startTime = System.currentTimeMillis();
        Type returnType = method.getGenericReturnType();
        log.info("ControllerAspect method: " + method.getName() + " start ..." + startTime);
        result = invocation.proceed();
//        } catch (Exception e) {
//            log.error("ControllerAspect method: "+method.getName()+"处理失败：", e);
//            return new BaseException(e.getMessage(),500);
//        } finally {
//
//        }
        log.info("ControllerAspect method: " + method.getName() + " end ..." + (System.currentTimeMillis() - startTime));
        return result;
    }


}
