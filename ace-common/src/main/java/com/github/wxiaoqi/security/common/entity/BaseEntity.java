package com.github.wxiaoqi.security.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@Getter
@Setter
public class BaseEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @JsonIgnore
    @Column(name = "locked")
    public Integer locked;

    @Column(name = "description")
    public String description;

    @JsonIgnore
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "crt_time")
    public Date crtTime;

    @JsonIgnore
    @Column(name = "crt_user")
    public String crtUser;

    @JsonIgnore
    @Column(name = "crt_name")
    public String crtName;

    @JsonIgnore
    @Column(name = "crt_host")
    public String crtHost;

    @JsonIgnore
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "upd_time")
    public Date updTime;

    @JsonIgnore
    @Column(name = "upd_user")
    public String updUser;

    @JsonIgnore
    @Column(name = "upd_name")
    public String updName;

    @JsonIgnore
    @Column(name = "upd_host")
    public String updHost;


}