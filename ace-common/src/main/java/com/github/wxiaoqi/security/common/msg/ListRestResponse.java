package com.github.wxiaoqi.security.common.msg;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-09 7:32
 */
public class ListRestResponse<T> extends BaseResponse {
    List<T> data;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public ListRestResponse data(List<T> data) {
        this.setData(data);
        return this;
    }
}
